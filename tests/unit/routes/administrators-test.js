import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | administrators', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:administrators');
    assert.ok(route);
  });
});
