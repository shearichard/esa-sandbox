# esa-sandbox

This project provides me with a place to experiment with [Ember Simple Auth](http://ember-simple-auth.com/) , in particular those aspects of it which are not covered by the [demo](https://ember-simple-auth.now.sh/) supplied with it (the [source for which is here](https://github.com/simplabs/ember-simple-auth/tree/master/tests/dummy)).

