import Route from '@ember/routing/route';
import ENV from 'drsdotnetclient/config/environment';

export default Route.extend({

  beforeModel: function(transition) {
    this.set('adapter', this.get('store').adapterFor('application'));
    this.set('host', this.get('store').adapterFor('application').get('host'));
    this.set('np', this.get('store').adapterFor('application').get('namespace'));
    this.set('urlbase', this.get('store').adapterFor('application').get('host') + '/' + this.get('store').adapterFor('application').get('namespace'));
  },

  model(params) {
    return new Promise(function (resolve, reject) {
      var theUrl = `${_that.urlbase}/costcentres/${_that.currentDRV.currentDRV}/${ENV.APP.DRSClientCode}/${targetCostCentreIdent}`;
      $.ajax({
        type: 'GET',
        url: theUrl,
        success: function (response) {
          resolve(response);
        },
        error: function (request, textStatus, error) {
          reject(error);
        }
      });
    })
  },
});
