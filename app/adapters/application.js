import WebApiAdapter from 'ember-web-api/adapters/web-api';
import ENV from 'drsdotnetclient/config/environment';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';

export default WebApiAdapter.extend(DataAdapterMixin, {
  host: ENV.host,
  namespace: 'drsdotnet/api',
  authorizer: 'authorizer:ddn-token-authorizer'
});
